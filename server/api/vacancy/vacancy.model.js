'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var VacancySchema = new mongoose.Schema({
  title: String,
  company: String,
  description: String,
  city: String,
  category: String,
  salaryRange: { title: String, from: Number, to: Number },
  email: String,
  salary: {
      type: Number,
      required: false
  }
});

export default mongoose.model('Vacancy', VacancySchema);
