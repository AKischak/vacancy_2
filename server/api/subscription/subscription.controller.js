/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subscriptions              ->  index
 * POST    /api/subscriptions              ->  create
 * GET     /api/subscriptions/:id          ->  show
 * PUT     /api/subscriptions/:id          ->  update
 * DELETE  /api/subscriptions/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Subscription from './subscription.model';
import Vacancy from './../vacancy/vacancy.model';
import config from './../../config/environment';
import SparkPost from 'sparkpost';

var sparkClient = new SparkPost(config['sparkpost']['apiKey']);

function respondWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function(entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}

function saveUpdates(updates) {
    return function(entity) {
        var updated = _.merge(entity, updates);
        return updated.saveAsync()
            .spread(updated => {
                return updated;
            });
    };
}

function removeEntity(res) {
    return function(entity) {
        if (entity) {
            return entity.removeAsync()
                .then(() => {
                    res.status(204).end();
                });
        }
    };
}

function handleEntityNotFound(res) {
    return function(entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        res.status(statusCode).send(err);
    };
}

// Gets a list of Subscriptions
export function index(req, res) {
    Subscription.findAsync()
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Gets a single Subscription from the DB
export function show(req, res) {
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Creates a new Subscription in the DB
export function create(req, res) {
    Subscription.createAsync(req.body)
        .then(respondWithResult(res, 201))
        .catch(handleError(res));
}

// Updates an existing Subscription in the DB
export function update(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(saveUpdates(req.body))
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Deletes a Subscription from the DB
export function destroy(req, res) {
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(removeEntity(res))
        .catch(handleError(res));
}


export function notify(req, res){
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(function(entity) {
            return notifySubscriber(entity);
        })
        .then(respondWithResult(res, 200))
        .catch(handleError(res));
}

export function notifyAll(req, res){
    notifyAllSubscribers()
        .then(() => {
            console.log('ok');
            return { status: 'ok' };
        })
        .then(respondWithResult(res, 200))
        .catch(handleError(res));
}

function notifySubscriber(entity){
    var query = { city: entity.city, category: { $in: entity.categories } };

    return new Promise((resolve, reject) => {
        Vacancy.findAsync(query).then((data) => {
            let promises = [];
            _.each(data, (v) => {
                var from = v.company + '<email>'.replace('email', v.email);
                var subject = "#TITLE# [#CITY#][#SALARY#][#COMPANY#]"
                    .replace('#TITLE#', v.title)
                    .replace('#CITY#', v.city)
                    .replace('#SALARY#', v.salaryRange.title)
                    .replace('#COMPANY#', v.company);

                let pr = new Promise((resolve, reject) => {
                    sparkClient.transmissions.send({
                        transmissionBody: {
                            content: {
                                reply_to: from,
                                from: 'localpart@sparkpostbox.com',
                                subject: subject,
                                html: v.description
                            },
                            recipients: [
                                { address: entity.email }
                            ]
                        }
                    }, function(err, res) {
                        if (err) {
                            console.log(err);
                            reject();
                        } else {
                            resolve();
                        }
                    });
                });
                promises.push(pr);
            });

            Promise.all(promises).then(resolve, reject);
        });
    });
}

function notifyAllSubscribers(){
    return new Promise((resolve, reject) => {
        let promises = [];
        Subscription.find().then((data) => {
            _.each(data, (entity) => {
                promises.push(notifySubscriber(entity));
            });
            Promise.all(promises).then(resolve, reject);
        });
    });
}