'use strict';

var express = require('express');
var controller = require('./subscription.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.delete('/:id', controller.destroy);
router.post('/notify/:id', controller.notify);
router.post('/notify_all', controller.notifyAll);

//router.get('/:id', controller.show);
//router.put('/:id', controller.update);
//router.patch('/:id', controller.update);


module.exports = router;
