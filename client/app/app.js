'use strict';

angular.module('dsApp', [
        'dsApp.constants',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'textAngular',
        'toaster'
    ])
    .config(['$urlRouterProvider', '$locationProvider', '$provide', function($urlRouterProvider, $locationProvider, $provide) {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);

        $provide.decorator('taOptions', ['$delegate', function(taOptions){
            // $delegate is the taOptions we are decorating
            // here we override the default toolbars and classes specified in taOptions.


            taOptions.toolbar = [
                //['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
                ['bold', 'italics', 'underline', 'ul', 'ol', 'redo', 'undo', 'clear'],
                ['justifyLeft','justifyCenter','justifyRight', 'justifyFull'],
                //['html', 'insertImage', 'insertLink', 'wordcount', 'charcount']
            ];
            return taOptions; // whatever you return will be the taOptions
        }]);
    }]);
