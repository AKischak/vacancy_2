'use strict';

angular.module('dsApp')
    .controller('SubscriptionCtrl', ['$scope', 'CategoryService',
        'CityService',
        'SalaryService',
        '$http',
        function ($scope, CategoryService, CityService, SalaryService, $http) {

        $scope.toggleCategorySelected = function(category){
            var index = $scope.model.categories.indexOf(category);

            if(index >= 0){
                $scope.model.categories.splice(index, 1);
            }else{
                if($scope.model.categories.length < 3){
                    $scope.model.categories.push(category);
                }
            }

            $scope.refreshViewSelectedCategories();
        };

        $scope.refreshViewSelectedCategories = function(){
            $scope.selectedCategories = $scope.model.categories ? $scope.model.categories.join(', ') : "";
        };

        $scope.submit = function(){
            $http({
                method: 'POST',
                url: '/api/subscriptions',
                data: $scope.model
            }).then((result) => {
                $scope.model = {
                    categories: [],
                    salaryRange: null,
                    city: null,
                    email: null
                };
            });
        };

        $scope.model = {
            categories: [],
            salaryRange: null,
            city: null,
            email: null
        };

        $scope.selectedCategories = "";

        $scope.categoryList = CategoryService.getAll();
        $scope.cityList = CityService.getAll();
        $scope.salaryList = SalaryService.getAll();

    }]);
