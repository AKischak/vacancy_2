'use strict';

angular.module('dsApp')
    .directive('subscriptionForm', [function(){
        return {
            restrict: "AE",
            controller: 'SubscriptionCtrl',
            scope: {},
            templateUrl: 'app/directives/subscription/subscription.html'
        };
    }]);
