'use strict';

angular.module('dsApp')
    .controller('ModalCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
        $scope.model = {};
        $scope.saveChanges = function(){
            $uibModalInstance.close($scope.model);
        };
        $scope.dismiss = function(){
            $uibModalInstance.dismiss();
        };
    }])
    .controller('DistributeCtrl', [
        '$scope',
        '$uibModal',
        'CategoryService',
        'CityService',
        'SalaryService',
        '$http',
        function ($scope, $uibModal, CategoryService, CityService, SalaryService, $http) {
            $scope.submit = function(){
                var modalInstance = $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    windowClass: 'custom-modal',
                    templateUrl: 'app/directives/distribute/modal.template.html',
                    controller: 'ModalCtrl',
                    controllerAs: '$ctrl',
                    size: 'lg',
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    backdropClass: 'custom-backdrop'
                });

                modalInstance.result.then(function (data) {
                    $http({
                        method: 'POST',
                        url: '/api/vacancy',
                        data: _.merge({}, $scope.model, data)
                    }).then((data) => {

                        $scope.model = {
                            category: null,
                            city: null,
                            salaryRange: null
                        };
                    });
                }, function () {});
        };


        $scope.model = {
            category: null,
            city: null,
            salaryRange: null
        };

        $scope.categories = CategoryService.getAll();
        $scope.cities = CityService.getAll();
        $scope.salaries = SalaryService.getAll();
    }]);
