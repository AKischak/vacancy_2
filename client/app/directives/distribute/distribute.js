'use strict';

angular.module('dsApp')
  .directive('distributeForm', [function(){
      return {
          restrict: "AE",
          controller: 'DistributeCtrl',
          scope: {},
          templateUrl: 'app/directives/distribute/distribute.html'
      };
  }]);
