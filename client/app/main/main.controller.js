'use strict';

(function() {

class MainController {

  constructor($http) {
    this.$http = $http;
  }

}

angular.module('dsApp')
  .controller('MainController', MainController);

})();
