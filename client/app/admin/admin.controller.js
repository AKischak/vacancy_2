'use strict';

angular.module('dsApp')
  .controller('AdminCtrl', [
      '$scope',
      '$http',
      'toaster',
  function ($scope, $http, toaster) {
      $http({
          method: 'GET',
          url: '/api/vacancy'
      }).then(function(response){
          $scope.vacancyList = response.data;
      });


      $scope.sendNotification2All = function(item){
          $http({
              method: 'POST',
              url: '/api/subscriptions/notify_all'
          }).then(function(response){
              toaster.pop('info', "All subscribers notified");
          }, function() {
              toaster.pop('error', "Error");
          });
      };

      $scope.removeVacancy = function(item){
          $http({
              method: 'DELETE',
              url: '/api/vacancy/' + item._id
          }).then(function(response){
              $scope.vacancyList = _.filter($scope.vacancyList, function(x){ return x._id != item._id; });
          });
      };

      $http({
          method: 'GET',
          url: '/api/subscriptions'
      }).then(function(response){
          $scope.subscriptionsList = response.data;
      });

      $scope.sendNotification = function(item){
          $http({
              method: 'POST',
              url: '/api/subscriptions/notify/'+item._id
          }).then(function(response){
              toaster.pop('success', "Notification sent");
          }, function() {
              toaster.pop('error', "Error");
          });
      };


      $scope.removeSubscription = function(item){
          $http({
              method: 'DELETE',
              url: '/api/subscriptions/' + item._id
          }).then(function(response){
              $scope.subscriptionsList = _.filter($scope.subscriptionsList, function(x){ return x._id != item._id; });
          });
      };
  }]);
