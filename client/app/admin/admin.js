'use strict';

angular.module('dsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin-panel',
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminCtrl'
      });
  });
