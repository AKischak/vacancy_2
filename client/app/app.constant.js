(function(angular, undefined) {
'use strict';

angular.module('dsApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);