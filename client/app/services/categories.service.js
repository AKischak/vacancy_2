'use strict';

angular.module('dsApp').service('CategoryService', [
    function(){


        this.getAll = function(){

            return ['Transportation',
                'Biotech',
                'Banking',
                'Economics',
                'Marketing'
            ];
        }
    }
]);