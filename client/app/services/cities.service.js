'use strict';

angular.module('dsApp').service('CityService', [
    function(){
        this.getAll = function(){
            return [ 'London', 'Manchester', 'Birmingham', 'Oxford', 'Liverpool' ];
        }
    }
]);