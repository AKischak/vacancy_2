'use strict';

angular.module('dsApp').service('SalaryService', [
    function(){
        this.getAll = function(){
            return [
                {
                    title:  'up to 200 000',
                    from: 0,
                    to: 200000
                },
                {
                    title: '201 000 - 400 000',
                    from: 201000,
                    to: 400000
                },
                {
                    title:  '401 000 - 600 000',
                    from: 401000,
                    to: 600000
                }
            ];
        }
    }
]);